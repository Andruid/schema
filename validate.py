import json
from pathlib import Path

import yaml
from jsonschema import validate
from pydantic import BaseModel, error_wrappers

from models.credentials_model import CredentialsModel
from models.manifest_model import ManifestModel
from models.transfer_model import TransferModel


def validate_yaml_to_model(file_url: Path) -> bool:

    if not file_url.is_file():
        raise FileNotFoundError

    with open(file_url) as file:
        file_content = yaml.load(file, Loader=yaml.SafeLoader)

    if str(file_content["kind"]).lower() == "credentials":
        try:
            CredentialsModel(**file_content)
            return True
        except error_wrappers.ValidationError as error:
            return False
    elif str(file_content["kind"]).lower() == "manifest":
        try:
            ManifestModel(**file_content)
            return True
        except error_wrappers.ValidationError as error:
            return False
    elif str(file_content["kind"]).lower() == "transfer":
        try:
            TransferModel(**file_content)
            return True
        except error_wrappers.ValidationError as error:
            return False
    else:
        raise FileNotFoundError


def validate_models_to_schema(model: BaseModel, schema_path: Path) -> bool:

    if not schema_path.is_file():
        raise FileNotFoundError

    with open(schema_path, 'r') as json_file:
        json_data = json.load(json_file)

    if json.loads(model.schema_json()) == json_data:
        return True
    else:
        return False


def validate_yaml_to_schema(yaml_path: Path, schema_path: Path):

    if not yaml_path.is_file():
        raise FileNotFoundError

    with open(yaml_path) as file:
        file_content = yaml.load(file, Loader=yaml.SafeLoader)

    if not schema_path.is_file():
        raise FileNotFoundError

    with open(schema_path) as schema:
        schema_content = yaml.load(schema, Loader=yaml.SafeLoader)

    try:
        validate(file_content, schema_content)
        return True
    except error_wrappers.ValidationError as error:
        raise error

# Hoppr Schemas Generator

## Overview

The Hoppr Schemas Generator CLI is a tool which validates and generates Hoppr Schema's

- Schema Validation against Hoppr Manifest File
- Schema Validation against Hoppr Credentials File
- Schema Validation against Hoppr Transfer File

__Usage:__

```bash
$ hoppr-schemas-generator -h
Usage of hoppr-schemas-generator:
  -generate-credentials-schema string
        Set -generate-credentials-schema=true to print out hoppr credentials schema file.
  -generate-manifest string
        Set -generate-manifest=true to print out hoppr manifest schema file.
  -generate-transfer-schema string
        Set -generate-transfer-schema=true to print out hoppr transfer schema.
  -v    Prints Version
  -validate-credentials string
        Specify the path to an hoppr credentials file to perform schema validation against
  -validate-manifest string
        Specify the path to an hoppr manifest file to perform schema validation against
  -validate-transfer string
        Specify the path to an hoppr transfer file to perform schema validation against
```


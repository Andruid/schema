## [1.1.7](https://gitlab.com/hoppr/schema/compare/v1.1.6...v1.1.7) (2023-01-13)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v9.0.2 ([7d72af2](https://gitlab.com/hoppr/schema/commit/7d72af2292c132c1a20979bab9f2c8c078b58caf))

## [1.1.6](https://gitlab.com/hoppr/schema/compare/v1.1.5...v1.1.6) (2023-01-08)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.0.2 ([202f4b9](https://gitlab.com/hoppr/schema/commit/202f4b92a1781e091e61adeaa11fcc3db403f0aa))

## [1.1.5](https://gitlab.com/hoppr/schema/compare/v1.1.4...v1.1.5) (2023-01-08)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.0.1 ([7397b0a](https://gitlab.com/hoppr/schema/commit/7397b0ae9553e671de481d1315651f0307a81f1e))

## [1.1.4](https://gitlab.com/hoppr/schema/compare/v1.1.3...v1.1.4) (2023-01-06)


### Bug Fixes

* **deps:** update dependency semantic-release to v20 ([d0b9675](https://gitlab.com/hoppr/schema/commit/d0b9675bbd4b57b41e3128c1e47b3cea9b4d03de))

## [1.1.3](https://gitlab.com/hoppr/schema/compare/v1.1.2...v1.1.3) (2022-12-20)


### Bug Fixes

* **deps:** update dependency @google/semantic-release-replace-plugin to v1.2.0 ([83178ae](https://gitlab.com/hoppr/schema/commit/83178ae8caf45d6656208b2b3f956271534007a5))

## [1.1.2](https://gitlab.com/hoppr/schema/compare/v1.1.1...v1.1.2) (2022-12-15)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9 ([59bc94a](https://gitlab.com/hoppr/schema/commit/59bc94a555d1932d7bb421c2977d4902a61429df))

## [1.1.1](https://gitlab.com/hoppr/schema/compare/v1.1.0...v1.1.1) (2022-12-15)


### Bug Fixes

* **deps:** update dependency conventional-changelog-conventionalcommits to v5 ([ca01196](https://gitlab.com/hoppr/schema/commit/ca011961a6a3f5268550e2de9f284f77e8c1e49a))
* **deps:** update dependency semantic-release-helm to v2.2.0 ([86e1c90](https://gitlab.com/hoppr/schema/commit/86e1c90e5dddeb77de20674142105772fabe32a0))
* **deps:** update dependency semantic-release-slack-bot to v3.5.3 ([72be23a](https://gitlab.com/hoppr/schema/commit/72be23a9098e480e2e869ab1e6c029951a5b6cca))
* **deps:** update semantic-release monorepo ([3536234](https://gitlab.com/hoppr/schema/commit/3536234aa78f135a68635c382fc014f3db72fc68))

## [1.1.0](https://gitlab.com/hoppr/schema/compare/v1.0.10...v1.1.0) (2022-12-14)


### Features

* update hoppr models to utilize pydantic models over golang ([a9d7cc1](https://gitlab.com/hoppr/schema/commit/a9d7cc14a9dadd160ba15dddcac118a702f7ddc8))


### Bug Fixes

* added gitignore to filter out .pyc files ([86120ba](https://gitlab.com/hoppr/schema/commit/86120baea91ae9ab6c4ce0ca7205f31619929718))
* change CI to use python ([3c346e9](https://gitlab.com/hoppr/schema/commit/3c346e9debc0c35c664c6d2aff47f5dafb3f3c8e))
* changed location for hoppr ([5092cc5](https://gitlab.com/hoppr/schema/commit/5092cc5a4efa68887e22e62ea8a183f379d18afc))
* removed schema files from folder ([f226e33](https://gitlab.com/hoppr/schema/commit/f226e338f672e4ae3cb1f149b1a9e5198d7519c5))
* resolve threads ([2a33fe4](https://gitlab.com/hoppr/schema/commit/2a33fe4320fc5ee125008845646e5b5340018896))

### [1.0.10](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.9...v1.0.10) (2022-07-21)


### Bug Fixes

* Update schema per !2 ([dbc1c04](https://gitlab.com/lmco/hoppr/schema/commit/dbc1c04b32efbcd5961bc2f37b89e35cb4595273))

### [1.0.9](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.8...v1.0.9) (2022-05-24)


### Bug Fixes

* update schema to use the resource object ([ce7b0a3](https://gitlab.com/lmco/hoppr/schema/commit/ce7b0a3c3289c0bb42485af582ecd3e60df03211))

### [1.0.8](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.7...v1.0.8) (2022-04-06)


### Bug Fixes

* Set allowed content to Capitalized first letter ([4ba14e7](https://gitlab.com/lmco/hoppr/schema/commit/4ba14e7332297b0201be62e84c066789a4f35ca4))
* Update credentials schema object ([f3c4829](https://gitlab.com/lmco/hoppr/schema/commit/f3c4829b24d7a370aa5d465966ac2ec5502ea230))

### [1.0.7](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.6...v1.0.7) (2022-04-01)


### Bug Fixes

* Release version, make static. ([c3b799f](https://gitlab.com/lmco/hoppr/schema/commit/c3b799fc6123a128eba8cbf720360d8a74a2c778))

### [1.0.6](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.5...v1.0.6) (2022-04-01)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v8.0.1 ([5e14dc6](https://gitlab.com/lmco/hoppr/schema/commit/5e14dc6bf60d93e73b3368589983fc567f98bcad))

### [1.0.5](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.4...v1.0.5) (2022-04-01)


### Bug Fixes

* Apply fix based on #note_891684017 from [@idunbarh](https://gitlab.com/idunbarh) ([0dcd536](https://gitlab.com/lmco/hoppr/schema/commit/0dcd53683a34ac95099ff6bb2821d250a0133032)), closes [#note_891684017](https://gitlab.com/lmco/hoppr/schema/issues/note_891684017)
* Clean up purls, Clean up Additional Properties ([7694587](https://gitlab.com/lmco/hoppr/schema/commit/769458724f1c723f990b458658236b7182c0373e))
* Merge branch 'main' into 'renovate/semantic-release-plus-docker-3.x' ([4435dac](https://gitlab.com/lmco/hoppr/schema/commit/4435dacc7d54c103ea4cfda87d5658c86187b323))
* Update manifest schema ([7ac6f3e](https://gitlab.com/lmco/hoppr/schema/commit/7ac6f3e731d68e1b5a7c2fd41cfa51e6cef23cc3))
* Update manifest schema ([a24413a](https://gitlab.com/lmco/hoppr/schema/commit/a24413a616dba50001785515c5076812032d0aa0))
* Update struct and api, add in comments ([5e5a526](https://gitlab.com/lmco/hoppr/schema/commit/5e5a5260d3a631e160cedf6d0ab54fedbc8268ab))
* Update struct and api, add in comments ([5c2ff6b](https://gitlab.com/lmco/hoppr/schema/commit/5c2ff6b9181784d85c294e5193bc0517b415e8eb))
* Update struct and api, add in comments ([c1ed0c7](https://gitlab.com/lmco/hoppr/schema/commit/c1ed0c783a2ee95ca25db853bde155346d67abef))

### [1.0.4](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.3...v1.0.4) (2022-04-01)


### Bug Fixes

* Apply fix based on #note_891684017 from [@idunbarh](https://gitlab.com/idunbarh) ([0dcd536](https://gitlab.com/lmco/hoppr/schema/commit/0dcd53683a34ac95099ff6bb2821d250a0133032)), closes [#note_891684017](https://gitlab.com/lmco/hoppr/schema/issues/note_891684017)
* Clean up purls, Clean up Additional Properties ([7694587](https://gitlab.com/lmco/hoppr/schema/commit/769458724f1c723f990b458658236b7182c0373e))
* Update manifest schema ([7ac6f3e](https://gitlab.com/lmco/hoppr/schema/commit/7ac6f3e731d68e1b5a7c2fd41cfa51e6cef23cc3))
* Update manifest schema ([a24413a](https://gitlab.com/lmco/hoppr/schema/commit/a24413a616dba50001785515c5076812032d0aa0))
* Update struct and api, add in comments ([5e5a526](https://gitlab.com/lmco/hoppr/schema/commit/5e5a5260d3a631e160cedf6d0ab54fedbc8268ab))
* Update struct and api, add in comments ([5c2ff6b](https://gitlab.com/lmco/hoppr/schema/commit/5c2ff6b9181784d85c294e5193bc0517b415e8eb))
* Update struct and api, add in comments ([c1ed0c7](https://gitlab.com/lmco/hoppr/schema/commit/c1ed0c783a2ee95ca25db853bde155346d67abef))

### [1.0.4](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.3...v1.0.4) (2022-03-31)


### Bug Fixes

* Apply fix based on #note_891684017 from [@idunbarh](https://gitlab.com/idunbarh) ([0dcd536](https://gitlab.com/lmco/hoppr/schema/commit/0dcd53683a34ac95099ff6bb2821d250a0133032)), closes [#note_891684017](https://gitlab.com/lmco/hoppr/schema/issues/note_891684017)
* Clean up purls, Clean up Additional Properties ([7694587](https://gitlab.com/lmco/hoppr/schema/commit/769458724f1c723f990b458658236b7182c0373e))
* Update manifest schema ([7ac6f3e](https://gitlab.com/lmco/hoppr/schema/commit/7ac6f3e731d68e1b5a7c2fd41cfa51e6cef23cc3))
* Update manifest schema ([a24413a](https://gitlab.com/lmco/hoppr/schema/commit/a24413a616dba50001785515c5076812032d0aa0))
* Update struct and api, add in comments ([5e5a526](https://gitlab.com/lmco/hoppr/schema/commit/5e5a5260d3a631e160cedf6d0ab54fedbc8268ab))
* Update struct and api, add in comments ([5c2ff6b](https://gitlab.com/lmco/hoppr/schema/commit/5c2ff6b9181784d85c294e5193bc0517b415e8eb))
* Update struct and api, add in comments ([c1ed0c7](https://gitlab.com/lmco/hoppr/schema/commit/c1ed0c783a2ee95ca25db853bde155346d67abef))

### [1.0.4](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.3...v1.0.4) (2022-03-29)


### Bug Fixes

* Apply fix based on #note_891684017 from [@idunbarh](https://gitlab.com/idunbarh) ([0dcd536](https://gitlab.com/lmco/hoppr/schema/commit/0dcd53683a34ac95099ff6bb2821d250a0133032)), closes [#note_891684017](https://gitlab.com/lmco/hoppr/schema/issues/note_891684017)
* Clean up purls, Clean up Additional Properties ([7694587](https://gitlab.com/lmco/hoppr/schema/commit/769458724f1c723f990b458658236b7182c0373e))
* Update struct and api, add in comments ([5e5a526](https://gitlab.com/lmco/hoppr/schema/commit/5e5a5260d3a631e160cedf6d0ab54fedbc8268ab))
* Update struct and api, add in comments ([5c2ff6b](https://gitlab.com/lmco/hoppr/schema/commit/5c2ff6b9181784d85c294e5193bc0517b415e8eb))
* Update struct and api, add in comments ([c1ed0c7](https://gitlab.com/lmco/hoppr/schema/commit/c1ed0c783a2ee95ca25db853bde155346d67abef))

### [1.0.3](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.2...v1.0.3) (2022-03-23)


### Bug Fixes

* Only cut releases on default branch ([0371aae](https://gitlab.com/lmco/hoppr/schema/commit/0371aaeffddfbcc64cdb315462ef309d876696af))
* Tidy up the go.mod file ([51477af](https://gitlab.com/lmco/hoppr/schema/commit/51477afc40d9c424310a12d1506aa769dd3b1992))

### [1.0.2](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.1...v1.0.2) (2022-03-23)


### Bug Fixes

* Upload files to generic repo ([aebd556](https://gitlab.com/lmco/hoppr/schema/commit/aebd55649e39afcfda50908fb9f8f6e6ce14fa15))

### [1.0.1](https://gitlab.com/lmco/hoppr/schema/compare/v1.0.0...v1.0.1) (2022-03-23)


### Bug Fixes

* Add pages artifacts ([d8a86f4](https://gitlab.com/lmco/hoppr/schema/commit/d8a86f432bb4dd4ebecf8c08828dad1ec0116bc2))
* Add pages artifacts ([8966685](https://gitlab.com/lmco/hoppr/schema/commit/8966685c66e921e292bbc7aa79d0bffdeca01347))
* Update package.json file for semantic ([f738477](https://gitlab.com/lmco/hoppr/schema/commit/f7384776c3af3816eab477288a6eeedb56ca301f))

## 1.0.0 (2022-03-23)


### Bug Fixes

* Add renovate, releaserc, and upload to pkg repo ([6b6d8c2](https://gitlab.com/lmco/hoppr/schema/commit/6b6d8c29a70c7c10fcb5c2355fb181fd3e3424fd))
* Initial commit for hoppr schemas generator ([279286b](https://gitlab.com/lmco/hoppr/schema/commit/279286bdba5bffeabd4c09e4a7fd52fb8a113b69))
* Pull in dependencies ([2a89540](https://gitlab.com/lmco/hoppr/schema/commit/2a89540b25808e9829b25f024c522b11a521a028))
* Update package.json file for semantic ([d9b77c4](https://gitlab.com/lmco/hoppr/schema/commit/d9b77c49724d07588ac28f8c292cae7afc4b4098))

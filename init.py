import os
from pathlib import Path

from models.credentials_model import CredentialsModel
from models.manifest_model import ManifestModel
from models.transfer_model import TransferModel
from validate import (validate_models_to_schema, validate_yaml_to_model,
                      validate_yaml_to_schema)


def generate_schemas(models_directory: Path):
    schema_path = models_directory.joinpath("schemas")
    if (not schema_path.exists()):
        os.mkdir(schema_path)
    with open(schema_path.joinpath("credentials-schema.json"), "w") as schema_file:
        schema_file.write(CredentialsModel.schema_json(indent=2))
    with open(schema_path.joinpath("manifest-schema.json"), "w") as schema_file:
        schema_file.write(ManifestModel.schema_json(indent=2))
    with open(schema_path.joinpath("transfer-schema.json"), "w") as schema_file:
        schema_file.write(TransferModel.schema_json(indent=2))


def validate_models():
    models = {
        "credentials": CredentialsModel,
        "manifest": ManifestModel,
        "transfer": TransferModel}

    for model in models.keys():
        print("test " + model + " yaml to model: " +
              str(validate_yaml_to_model(Path("test", "resources", model + ".yml"))))
        print("test " + model + " yaml to schema: " + str(validate_yaml_to_schema(Path("test",
              "resources", model + ".yml"), Path("models", "schemas", model + "-schema.json"))))
        print("test " + model + " model to schema: " + str(validate_models_to_schema(
            models[model], Path("models", "schemas", model + "-schema.json"))))


def main():
    generate_schemas(Path("models"))
    validate_models()


if __name__ == "__main__":
    main()
